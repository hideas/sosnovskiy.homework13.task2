﻿namespace Sosnovskiy.Homework13.Task2
{
    partial class Checks
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Checks));
            this.titleBlinking = new System.Windows.Forms.Timer(this.components);
            this.cb1 = new System.Windows.Forms.CheckBox();
            this.cb2 = new System.Windows.Forms.CheckBox();
            this.cb3 = new System.Windows.Forms.CheckBox();
            this.cb4 = new System.Windows.Forms.CheckBox();
            this.cb5 = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // titleBlinking
            // 
            this.titleBlinking.Interval = 200;
            this.titleBlinking.Tick += new System.EventHandler(this.titleBlinking_Tick);
            // 
            // cb1
            // 
            this.cb1.AutoSize = true;
            this.cb1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cb1.Location = new System.Drawing.Point(106, 37);
            this.cb1.Name = "cb1";
            this.cb1.Size = new System.Drawing.Size(32, 17);
            this.cb1.TabIndex = 0;
            this.cb1.Text = "1";
            this.cb1.UseVisualStyleBackColor = true;
            this.cb1.Click += new System.EventHandler(this.cb_Click);
            // 
            // cb2
            // 
            this.cb2.AutoSize = true;
            this.cb2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cb2.Location = new System.Drawing.Point(106, 73);
            this.cb2.Name = "cb2";
            this.cb2.Size = new System.Drawing.Size(32, 17);
            this.cb2.TabIndex = 1;
            this.cb2.Text = "2";
            this.cb2.UseVisualStyleBackColor = true;
            this.cb2.Click += new System.EventHandler(this.cb_Click);
            // 
            // cb3
            // 
            this.cb3.AutoSize = true;
            this.cb3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cb3.Location = new System.Drawing.Point(106, 109);
            this.cb3.Name = "cb3";
            this.cb3.Size = new System.Drawing.Size(32, 17);
            this.cb3.TabIndex = 2;
            this.cb3.Text = "3";
            this.cb3.UseVisualStyleBackColor = true;
            this.cb3.Click += new System.EventHandler(this.cb_Click);
            // 
            // cb4
            // 
            this.cb4.AutoSize = true;
            this.cb4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cb4.Location = new System.Drawing.Point(106, 145);
            this.cb4.Name = "cb4";
            this.cb4.Size = new System.Drawing.Size(32, 17);
            this.cb4.TabIndex = 3;
            this.cb4.Text = "4";
            this.cb4.UseVisualStyleBackColor = true;
            this.cb4.Click += new System.EventHandler(this.cb_Click);
            // 
            // cb5
            // 
            this.cb5.AutoSize = true;
            this.cb5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cb5.Location = new System.Drawing.Point(106, 181);
            this.cb5.Name = "cb5";
            this.cb5.Size = new System.Drawing.Size(32, 17);
            this.cb5.TabIndex = 4;
            this.cb5.Text = "5";
            this.cb5.UseVisualStyleBackColor = true;
            this.cb5.Click += new System.EventHandler(this.cb_Click);
            // 
            // Checks
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(260, 237);
            this.Controls.Add(this.cb5);
            this.Controls.Add(this.cb4);
            this.Controls.Add(this.cb3);
            this.Controls.Add(this.cb2);
            this.Controls.Add(this.cb1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Checks";
            this.Load += new System.EventHandler(this.Checks_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer titleBlinking;
        private System.Windows.Forms.CheckBox cb1;
        private System.Windows.Forms.CheckBox cb2;
        private System.Windows.Forms.CheckBox cb3;
        private System.Windows.Forms.CheckBox cb4;
        private System.Windows.Forms.CheckBox cb5;
    }
}


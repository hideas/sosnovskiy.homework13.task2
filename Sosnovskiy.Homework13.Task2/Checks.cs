﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sosnovskiy.Homework13.Task2
{
    public partial class Checks : Form
    {
        // List of checkboxes field
        private List<CheckBox> checkBoxes;

        public Checks()
        {
            InitializeComponent();

            // Setting list of checkboxes
            checkBoxes = this.Controls.OfType<CheckBox>().ToList();
        }

        private async void Checks_Load(object sender, EventArgs e)
        {
            // Setting text blink on form title for 3s
            titleBlinking.Start();
            await Task.Delay(3000);
            titleBlinking.Dispose();

            this.Text = "Checks";
        }

        private void titleBlinking_Tick(object sender, EventArgs e)
        {
            this.Text = this.Text.Equals("Set all the checks!") ? " " : "Set all the checks!";
        }

        private void cb_Click(object sender, EventArgs e)
        {
            CheckBox checkBox = (CheckBox)sender;

            // Getting random two checkboxes from list
            var selectedCheckBoxes =
                (
                    from item in checkBoxes
                    where !item.Equals(checkBox)
                    select item
                ).OrderBy(item => Guid.NewGuid()).Take(2).ToList(); // Guid.NewGuid()

            // If current checkBox checked
            if (checkBox.Checked)
            {
                // Check or uncheck other random two
                foreach (CheckBox item in selectedCheckBoxes)
                {
                    if (!item.Checked)
                    {
                        item.Checked = true;
                    }
                    else
                    {
                        item.Checked = false;
                    }
                }
            }
            // Else do not uncheck
            else
            {
                checkBox.Checked = true;
            }

            // If all checkboxes check - call victory method
            if (checkVictory())
            {
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult result = MessageBox.Show("You win!", "Restart?", buttons);

                if (result == DialogResult.Yes)
                {
                    Application.Restart();
                }
                else
                {
                    this.Close();
                }
            }
        }

        private bool checkVictory()
        {
            bool allChecked = true;

            foreach (CheckBox item in checkBoxes)
            {
                if (!item.Checked)
                {
                    allChecked = false;
                    break;
                }
            }
            return allChecked;
        }
    }
}
